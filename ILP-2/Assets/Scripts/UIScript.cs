using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScript : MonoBehaviour
{
    public RectTransform UIScale;
    public AnimationCurve UICurve;

    public float expandAmount;

    private float scaleTime;
    private Vector3 startSize;
    private Vector3 targetSize;


    private void Start()
    {
        startSize = UIScale.localScale;
        targetSize = startSize * expandAmount;
    }

    // Update is called once per frame
    void Update()
    {
        //time scale
        scaleTime += Time.deltaTime;
        float _percent = UICurve.Evaluate(scaleTime);

        //UI pulse scale
        UIScale.localScale = Vector3.Lerp(startSize, targetSize, _percent);      
    }
}
