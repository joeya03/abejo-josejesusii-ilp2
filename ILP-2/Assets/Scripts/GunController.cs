using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    //get reference from componens and objects
    public Transform firePoint;
    public Transform pivotPoint;
    public GameObject bulletPre;
    public GameObject crosshair;

    private Animator anim;

    [SerializeField] private float shootDelay = .25f;
    public float bulletForce = 20f;



    // Start is called before the first frame update
    void Start()
    {
        anim = GameObject.Find("gun").GetComponent<Animator>();
    }//end of Start

    // Update is called once per frame
    void Update()
    {
        //cursor position
        Transform cursorPosition = crosshair.GetComponent<Transform>();
        cursorPosition.transform.position = GameObject.Find("Main Camera").GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition) + new Vector3(0f, 0f, 10f);

        //shoot delay
        shootDelay -= Time.deltaTime;

        //if player shoots with left mouse button, makes sure player cant spam shoot
        if (Input.GetButtonDown("Fire1") && shootDelay <= 0f)
        {
                //anim.SetTrigger("playerShoot");
                anim.Play("Shoot");
                Shoot();
                shootDelay = .25f;  
        }
    }//end of update

    //player shooting
    void Shoot() 
    {
        //instantiate bullet
        GameObject bullet = Instantiate(bulletPre, firePoint.position, pivotPoint.rotation);

        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        //shoot bullet towards angled target
        rb.AddForce(bulletForce * firePoint.right, ForceMode2D.Impulse);
    }//end of Shoot
}
