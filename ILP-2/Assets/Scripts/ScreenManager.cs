using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenManager : MonoBehaviour
{
    public string LevelToLoad;
    public void LoadScene() 
    {
        Cursor.visible = true;
        SceneManager.LoadScene(LevelToLoad);
    }

}
 