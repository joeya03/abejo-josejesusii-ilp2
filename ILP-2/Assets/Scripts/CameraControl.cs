using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    //Declaring Variables and Components
    public Transform followTarget;
    [SerializeField] private Camera cam;

    public float targetSpeed = 0.125f;
    public Vector3 offset = new Vector3(0f,0f,-10f);
    private Vector3 velocity = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        //grabbing components
        cam = gameObject.GetComponent<Camera>();
    }//end of Start

    // Update is called once per frame
    void Update()
    {
        //grab location of cursor
        Vector3 pointer = cam.ScreenToWorldPoint(Input.mousePosition);
        Vector3 targetPosition = Vector3.zero;

        //make sure that mouse is in game view
        if (!(0 > Input.mousePosition.x || 0 > Input.mousePosition.y || Screen.width < Input.mousePosition.x || Screen.height < Input.mousePosition.y))
        {
            //find the 1/5th point between two positions
            targetPosition = new Vector3(followTarget.position.x + (pointer.x - followTarget.position.x) / 5, followTarget.position.y + (pointer.y - followTarget.position.y) / 5, -10);
            Debug.Log("on screen");
            Cursor.visible = false;
        }
        //cursor is off screen
        else 
        {
            targetPosition = followTarget.position + offset;
            Debug.Log("out of screen");
            Cursor.visible = true;
        }
        
        //camera follow
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, targetSpeed);

    }//end OF Update
}
