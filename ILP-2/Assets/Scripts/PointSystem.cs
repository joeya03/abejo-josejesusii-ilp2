using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointSystem : MonoBehaviour
{
    //Declaring Variables and Components
    public Text points;

    // Start is called before the first frame update
    void Start() 
    {
        Cursor.visible = true;

        //show player points
        int pointsSys = PlayerPrefs.GetInt("TotalPoints");
        points.text = "Points: " + pointsSys;
    }//end of start

}
