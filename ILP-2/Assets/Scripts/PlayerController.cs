using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    //Declaring Variables and Components
    [SerializeField] private Transform pivotPoint;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Camera cam;
    [SerializeField] private Animator anim;
    [SerializeField] private SpriteRenderer render;

    public GameObject deathEffect;
    public Text health;

    public int playerHealth = 200;
    
    public float movementSpeed = 5f;

    Vector2 movement;

    // Start is called before the first frame update
    private void Start()
    {
        //grab components from game objects
        pivotPoint = GameObject.Find("PivotPoint").GetComponent<Transform>();
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        anim = gameObject.GetComponent<Animator>();
        render = gameObject.GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        //Player Input
        PlayerInput();

        health.text = "Health: " + playerHealth;
    }//end of update

    // Physics Update
    void FixedUpdate()
    {
        //Player Movement
        rb.MovePosition(rb.position + movement * movementSpeed * Time.fixedDeltaTime);
        //Gun Movement
        GunRotation();
    }//end of FixedUpdate

    // Player input
    void PlayerInput() 
    {
        //Player Movement Input
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        //set character move animation
        if (movement.x != 0 || movement.y != 0)
            anim.SetBool("playerMove", true);
        else
            anim.SetBool("playerMove", false);

        //Player Mouse position and Input
        //cam.ScreenToWorldPoint(Input.mousePosition);
    }//end of PlayerInput

    // gun rotation
    void GunRotation() 
    {
        //Gun Movement
        Vector3 diff = (cam.ScreenToWorldPoint(Input.mousePosition) - pivotPoint.position).normalized;

        float rotation = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg ;
        Debug.Log(rotation);

        //pivot rotation
        pivotPoint.rotation = Quaternion.Euler(0f, 0f, rotation);

        //change player order layer
        if (rotation > 10 && rotation < 170)
            render.sortingOrder = 2;
        else
            render.sortingOrder = 0;

        //flip gun model based on position
        Vector3 pivotScale = pivotPoint.localScale;

        //looking left
        if (rotation >= 90 || rotation <= -90)
        {
            render.flipX = true;
            pivotScale.y = -0.75f;
        }
        //looking right
        else
        {
            render.flipX = false;
            pivotScale.y = 0.75f;
        }
        pivotPoint.transform.localScale = pivotScale;
        CharacterAnimation(rotation);
    }//end of GunRotation

    // The character will change direction based on the position of the mouse(position of the gun)
    void CharacterAnimation(float rotateDeg) 
    {
        //looking down/towards (-135, -45)
        if (rotateDeg >= -135 && rotateDeg <= -45) 
            anim.SetFloat("rotation", 1);
        //looking left (-135 - -180, 135 - 180)
        else if ((rotateDeg <= -135 && rotateDeg >= -180) || (rotateDeg >= 135 && rotateDeg <= 180))
            anim.SetFloat("rotation", 2);  
        //looking right (-45, 45)
        else if ((rotateDeg <= 45 && rotateDeg >= -45))
            anim.SetFloat("rotation", 2);
        //looking up (45, 135)
        else if (rotateDeg <= 135 && rotateDeg >= 45)
            anim.SetFloat("rotation", 3);

    }//end of CharacterAnimation

    // Occurs when player is hurt from an enemy.
    public void PlayerHurt(int damage) 
    {
        playerHealth -= damage;
        if (playerHealth <= 0) 
        {
            Death();
        }
    }//end of PlayerHurt

    //when player dies
    void Death() 
    {
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Cursor.visible = true;
        //make a playerpref for their total points
        PlayerPrefs.SetInt("TotalPoints", GameObject.Find("Spawner").GetComponent<GameManager>().points);
        gameObject.SendMessage("LoadScene");
    
    }//end of Death

}
