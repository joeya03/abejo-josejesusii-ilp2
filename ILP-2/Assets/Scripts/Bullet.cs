using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    //Declaring Variables and Components
    public Rigidbody2D rb;
    public GameObject impactEffect;

    public int damage = 25;

    void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag) 
        {
            case "Walls":
                // Wall collision
                Impact();
                break;
            case "Enemy":
                // Enemy Collision
                collision.GetComponent<Enemy>().TakeDamage(damage);
                Impact();
                break;
            default:
                break;
        }
    }//end of OnTriggerEnter2D

    //when bullet impacts a certain collision trigger.
    public void Impact() 
    {
        Instantiate(impactEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }//End of Impact

}
