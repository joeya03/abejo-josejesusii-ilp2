using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    //Declaring Variables and Components
    public int health = 100;
    public GameObject deathEffect;
    [SerializeField] private SpriteRenderer render;

    private float damageTimer = 0f;

    public float speed;
    private Transform playerPosition;

    // Start is called before the first frame update
    void Start()
    {
        playerPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        render = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        damageTimer -= Time.deltaTime;
        if (playerPosition.position.x > this.transform.position.x)
            render.flipX = false;
        else
            render.flipX = false;

        ///enemy will move towards player
        transform.position = Vector2.MoveTowards(transform.position, playerPosition.position, speed * Time.deltaTime);
        //Debug.Log(Vector2.MoveTowards(transform.position, playerPosition.position, speed * Time.deltaTime));
    }

    // Whenever Enemy takes damage
     public void TakeDamage(int damage) 
    {
        health -= damage;

        if (health <= 0)
            Death();
    }//end of TakeDamage

    // Occurs when Enemy takes certain amount of damage, Deletes enemy object
    void Death() 
    {
        //spawn death particles
        Instantiate(deathEffect, transform.position, Quaternion.identity);

        //send point message to Script
        GameObject.Find("Spawner").SendMessage("AddPoints", 10);

        //destroy enemy
        Destroy(gameObject);
    }//End of Death

    private void OnCollisionStay2D(Collision2D collision)
    {
        //if its the player collition and hit timer is at 0
        if (collision.gameObject.CompareTag("Player") && damageTimer <= 0f)
        {
            //damage the player
            collision.gameObject.GetComponent<PlayerController>().PlayerHurt(25);
            damageTimer = 3f;
        }
    }//End of OnCollisionEnter2D

}
