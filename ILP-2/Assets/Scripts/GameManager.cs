using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    //Declaring Variables and Components
    public GameObject enemy;
    public Transform[] spawnSpots;
    private float timebetweenSpawn;
    public float startTimeBetweenSpawn;
    public Text pointsSystem;
    public int points = 0;

    // Start is called before the first frame update
    void Start()
    {
        timebetweenSpawn = startTimeBetweenSpawn;
    }//end of start

    // Update is called once per frame
    void Update()
    {
        //makes sure that there are oonly 15 enemies on screen
        if (GameObject.FindGameObjectsWithTag("Enemy").Length <= 8) 
        {
            //spawns a slime on a random spot every time between spawn.
            if (timebetweenSpawn <= 0)
            {
                int randomPos = Random.Range(0, spawnSpots.Length);
                Instantiate(enemy, spawnSpots[randomPos].position, Quaternion.identity);
                timebetweenSpawn = startTimeBetweenSpawn;
            }
            else
            {
                timebetweenSpawn -= Time.deltaTime;
            }
        }
    }//end of update

    public void AddPoints(int pointsAdd)
    {
        points += pointsAdd;
        pointsSystem.text = "Points: " + points;
    }//end of AddPoints

}
